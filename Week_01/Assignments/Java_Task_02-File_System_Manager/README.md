# File System Manager :file_folder:

A File System Manager written in Java, which can interact with files in the resources directory of the project.

## Functionality:

* List all files in the directory
* List files in the directory by extension
* Manipulate a .txt file in the folder:
  * Get the name of text file
  * Get the size of text file
  * Check the number of lines in the text file
  * Check if a given word exists in the text file
  * Check how many times a given word appears in the text file
* Logging
  * The program logs the result of every function along with a timestamp and the functions execution time

## How to run program?

* Clone the project or Download the source code for `Java_Task_02-File_System_Manager`
* Open `Java_Task_02-File_System_Manager` directory in editor
* Open terminal so you are in `Java_Task_02-File_System_Manager` directory
* Go into `out` directory in terminal by typing `cd out`
* Run the program by typing `java main/Program` in the terminal from `out` directory

## Screenshots:

* compiling the project:

    ![Screenshot 1](/Week_01/Assignments/Java_Task_02-File_System_Manager/src/screenshots/screen1.png "Screen 1")

* creating the .jar file:

    ![Screenshot 2](/Week_01/Assignments/Java_Task_02-File_System_Manager/src/screenshots/screen2.png "Screen 2")

* running the .jar file:

    ![Screenshot 3](/Week_01/Assignments/Java_Task_02-File_System_Manager/src/screenshots/screen3.png "Screen 3")


