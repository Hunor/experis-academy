package main;

import services.file_manipulation.FileManipulation;
import services.user_inferface.UserInterface;

public class Program {
    public static void main(String[] args) {

        MainMenu();
    }
    
    // Main Menu
    private static void MainMenu(){
        System.out.println();
        System.out.println("Welcome to the Java File System Manager!");
        System.out.println("How can I assist You?\n");

        System.out.println();
        System.out.println("1. List all the files in the directory.");
        System.out.println("2. List all the files with the same extension.");
        System.out.println("3. Get a specific information about the text file.");
        System.out.println("4. Exit the program.");
         
        System.out.println("Please enter your choice: ");

        FileManipulation fileManipulation = new FileManipulation();
        UserInterface userInterface = new UserInterface();
        String userChoice = UserInterface.getUserInput();
        
        switch(userChoice){
            case "1":
                fileManipulation.listAllFilesInDirectory();
                MainMenu();
                break;
            case "2":
                userInterface.getFileExtension();
                MainMenu();
                break;
            case "3":
                SecondaryMenu();
                MainMenu();
                break;
            case "4":
                return;
            default:
                System.out.println();
                System.out.println("Sorry, choose an option from the list!");
                MainMenu();
        }
    }

    // Secondary Menu
    private static void SecondaryMenu(){
        System.out.println();
        System.out.println("1. Get the name of the text file.");
        System.out.println("2. Get the size of the text file.");
        System.out.println("3. Get the total number of lines in text file.");
        System.out.println("4. Check if a word exists in the file.");
        System.out.println("5. Check how many times a word occurs in the file.");
        System.out.println("6. Return to Main Menu.");

        System.out.println("Please enter your choice: ");

        FileManipulation fileManipulation = new FileManipulation();

        String userChoice = UserInterface.getUserInput();

        switch(userChoice){
            case "1":
                fileManipulation.getFileName();
                SecondaryMenu();
                break;
            case "2":
                fileManipulation.getFileSize();
                SecondaryMenu();
                break;
            case "3":
                fileManipulation.getTotalNumberOfLines();
                SecondaryMenu();
                break;
            case "4":
                fileManipulation.checkIfWordExists(UserInterface.getUserInput());
                SecondaryMenu();
                break;
            case "5":
                fileManipulation.checkWordOccurance(UserInterface.getUserInput());
                SecondaryMenu();
                break;
            case "6":
                return;
            default:
                System.out.println("Sorry, choose an option from the list!");
                SecondaryMenu();
                break;
        }
    }
}