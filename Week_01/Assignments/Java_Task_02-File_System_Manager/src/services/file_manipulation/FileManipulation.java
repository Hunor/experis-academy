package services.file_manipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.Scanner;

import services.logging.*;

public class FileManipulation {

    Logging logging = new Logging();

    // GET all files in the directory
    public void listAllFilesInDirectory(){
        try {
            logging.startCounting();

            String[] pathnames;
        
            File f = new File("../src/files");
            pathnames = f.list();
    
            for (String pathname : pathnames) {
    
                System.out.println(pathname);
            }
            logging.stopCounting();
            logging.textLogger("Printig all the files in the directory");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // GET all files with same extension in the directory
	public void listAllFilesByExtension(String extension) {
        try {
            logging.startCounting();
            File f = new File("../src/files");

            File[] listFiles = f.listFiles(new MyFileNameFilter(extension));
    
            for (File file: listFiles){
                System.out.println();
                System.out.println("File: " + file.getName());
                System.out.println();
            }
            logging.stopCounting();
            logging.textLogger("Getting all the files with the same extension");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
	}

    // Helper method for listAllFilesByExtension() method
	public static class MyFileNameFilter implements FilenameFilter {
		private String extension;

		public MyFileNameFilter(String extension) {
			this.extension = extension.toLowerCase();
		}

		@Override
		public boolean accept(File dir, String name) {
			return name.toLowerCase().endsWith(extension);
		}
	}

	// GET the name of the text file
	public void getFileName() {
        try {
            logging.startCounting();

            File f = new File("../src/files/Dracula.txt");
            System.out.println(); 
            System.out.println("The name of the file is: " + f.getName());  
            System.out.println();   
            
            logging.stopCounting();
            logging.textLogger("Getting the name of the file");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }      
    }
    
    // GET the size of the text file
	public void getFileSize() {
        try {
            logging.startCounting();

            File f = new File("../src/files/Dracula.txt");
        
            // Get length of file in bytes
            long fileSizeInBytes = f.length();
            System.out.println(); 
            System.out.println("The size of the file is: " + fileSizeInBytes + " Bytes"); 
            System.out.println(); 
    
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            long fileSizeInKB = fileSizeInBytes / 1024;
            System.out.println(); 
            System.out.println("The size of the file is: " + fileSizeInKB + " KiloBytes");  
            System.out.println(); 

            logging.stopCounting();
            logging.textLogger("Getting the size of the text file");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    // GET the total number of lines in the text file
	public void getTotalNumberOfLines() {
	    try {
            logging.startCounting();

            File f = new File("../src/files/Dracula.txt");

            Scanner scanner = new Scanner(new FileReader(f));
            int noOfLines = 0;
	        while (scanner.hasNextLine()) {
	            scanner.nextLine();
	            noOfLines++;
            }
            System.out.println(); 
            System.out.println("The number of lines in the text is: " + noOfLines);  
            System.out.println(); 

            logging.stopCounting();
            logging.textLogger("Getting the total number of lines in the text file");
	    } catch(Exception e) {
	        System.out.println(e.getMessage());  
	    }
    }
    
    // check if given word is in the text
	public  void checkIfWordExists(String input) {
        try {
            logging.startCounting();

            File f = new File("../src/files/Dracula.txt");
            String[] words = null;  //Intialize the word Array
            FileReader fr = new FileReader(f);  //Creation of File Reader object
            BufferedReader br = new BufferedReader(fr); //Creation of BufferedReader object
            String s;     
            int count=0;   //Intialize the word to zero
            while((s=br.readLine())!=null)   //Reading Content from the file
            {
                s.toLowerCase();
                words=s.split(" ");  //Split the word using space
            
                for (String word : words) 
                {
                    if (word.equals(input))   //Search for the given word
                    {
                        count++;    // If Present increase the count by one
                    }
                }
            }
            
            if(count!=0){
                System.out.println();
                System.out.println("The given word is present in the text file");
                System.out.println();
            } else {
                System.out.println();
                System.out.println("The given word is not present in the text file");
                System.out.println();
            }
            
            fr.close();

            logging.stopCounting();
            logging.textLogger("Checking if the given word is present in the text file");
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
      }

    // count how many times a word appears in the text
	public void checkWordOccurance(String input) {
        try {
            logging.startCounting();

            File f = new File("../src/files/Dracula.txt");
            String[] words = null;  //Intialize the word Array
            FileReader fr = new FileReader(f);  //Creation of File Reader object
            BufferedReader br = new BufferedReader(fr); //Creation of BufferedReader object
            String s;     
            int count=0;   //Intialize the word to zero
            while((s=br.readLine())!=null)   //Reading Content from the file
            {
                s.toLowerCase();
                words=s.split(" ");  //Split the word using space
            
                for (String word : words) 
                {
                    if (word.equals(input))   //Search for the given word
                    {
                        count++;    // If Present increase the count by one
                    }
                }
            }
            
            if(count!=0){
                System.out.println();
                System.out.println("The given word is " + count + " times present in the text file");
                System.out.println();
            } else {
                System.out.println();
                System.out.println("The given word is not present in the text file");
                System.out.println();
            }
            
            fr.close();

            logging.stopCounting();
            logging.textLogger("Checking the occurance of the given word");
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
