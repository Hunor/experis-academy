package services.logging;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Logging {

    long start;
    int duration;
    public void startCounting(){
        start = System.nanoTime();
    }

    public void stopCounting(){
        long finish = System.nanoTime();
        long executionTime = (finish - start) / 100000;
        duration = Math.toIntExact(executionTime);
    }

    public String getCurrentDateAndTime(){
        SimpleDateFormat formater= new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String dateFormat = formater.format(date);
        return dateFormat;
    }
    public void textLogger(String text){
        try {
            FileWriter writer = new FileWriter("../src/log_text/logging.txt", true);
            writer.write(text + " took " + duration + " ms to execute." + "\nCurrent date and time is: " + getCurrentDateAndTime() + "\n\n");
            writer.close();
        }
        catch(IOException e){
            System.out.println(e);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
