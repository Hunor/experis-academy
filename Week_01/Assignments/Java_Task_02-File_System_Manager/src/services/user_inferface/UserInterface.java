package services.user_inferface;

import java.util.Scanner;

import services.file_manipulation.FileManipulation;

public  class UserInterface {
    FileManipulation fileManipulation = new FileManipulation();
    public static String getUserInput(){
        try {
            Scanner scanner = new Scanner(System.in);
            String userInput = scanner.nextLine();
            return userInput.trim();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void getFileExtension(){
        System.out.println("Choose a file extension (txt, jpeg, jpg, jfif): ");
        String extension = getUserInput();
        fileManipulation.listAllFilesByExtension(extension);
    }
}
