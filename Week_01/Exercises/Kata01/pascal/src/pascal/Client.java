package pascal;



public class Client {

	public static void main(String[] args) {

        //String str = "The quick brown fox jumped over the lazy dog"; 
        String str = "The qUick!  bRoWn fox    jumped, OVER the    lazy. dog";
        String nameWithProperSpacing = str.replaceAll("\\s+", " ");
        String nameWithoutPunctuation = nameWithProperSpacing.replaceAll("[^a-zA-Z ]", "");
        
        //System.out.println( nameWithProperSpacing );

        //System.out.println( nameWithoutPunctuation );

        str = PascalConverter(nameWithoutPunctuation); 
  
        System.out.println(str); 
	}
	
    public static String PascalConverter(String str) 
    { 
        // Capitalize first letter of string 
        str = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase(); 
  
        // Convert to StringBuilder 
        StringBuilder builder = new StringBuilder(str); 
  
        // Traverse the string character by 
        // character and remove underscore 
        // and capitalize next letter 
        for (int i = 0; i < builder.length(); i++) { 
  
            // Check char is space 
            if (builder.charAt(i) == ' ') { 
  
                builder.deleteCharAt(i); 
                builder.replace( i, i + 1, String.valueOf( Character.toUpperCase( builder.charAt(i)))); 
            } 
        } 

        return builder.toString(); 
    } 

}
