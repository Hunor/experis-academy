package largest_difference_in_array;

public class Client {

	public static void main(String[] args) {
		// array of type integers
		int[] numbers = { 2, 3, 1, 7, 9, 5, 11, 3, 5 };
		System.out.println(1-2);
		// get the number of elements of the array
        int n = numbers.length;

        // variables to store array element with the highest and lowest value
        // starting from the first element which is [0] in our case
        int smallestElement = numbers[0]; 
        int higestElement = numbers[0]; 
        
        // loop through the array elements 
        // compare the elements and store the highest and lowest
        for (int i = 1; i < n; i++) { 
        	// compare the next element in the array with the smallest one stored in the variable
        	smallestElement = Math.min(smallestElement, numbers[i]); 
        	
        	// compare the next element in the array with the highest one stored in the variable
        	higestElement = Math.max(higestElement, numbers[i]); 
        } 
  
        System.out.println("The difference between the biggest and smallest "
        		+ "element in the array is: "
        		+ (higestElement - smallestElement));
        
	}

}
