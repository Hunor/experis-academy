package largest_difference_in_array2;

public class Client {

	public static void main(String[] args) {
		int[] numbers = { 2, 3, 1, 7, 9 };
		getLargestDifference(numbers);
	}
	
	public static void getLargestDifference(int[] numbers) {
	int maxDiff = numbers[1] - numbers[0];
	int minVal = numbers[0];
	
	for(int i = 0; i < numbers.length; i++) {
		if(numbers[i] - minVal > maxDiff) {
			maxDiff = numbers[i] - minVal;
		}
		
		if(numbers[i] < minVal) {
			minVal = numbers[i];
		}
	}
	System.out.println(maxDiff);
	//return maxDiff;
	}
}
