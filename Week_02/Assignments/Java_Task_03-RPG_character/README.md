# RPG characters :european_castle:

An RGP character system written in Java. The system represents various software design paradigms and design patterns 

## Functionality:

* Characters can created with basic starting points. Their attributes increase at higher levels. 
  * Character types: Warrior, Mage, Ranger
* Items suchs as different kinds of weapons and armors with basic starting points can be created.
  * Weapon types: Magic, Melee, Ranged
    * Weapons: Sword, Bow, Thunderbolt
  * Armors: Cloth, Leather, Plate 
* Items such as weapons and armors can be added to a specific character which alters the basic stats of the character

## Screenshots:

* creating a hero of type Warrior with basic stats:

    ![Screenshot 1](/Week_02/Assignments/Java_Task_03-RPG_character/screenshots/scr1.png "Screen 1")

* adding a sword and a plate to the warrior and updating the stats of the warrior:

    ![Screenshot 2](/Week_02/Assignments/Java_Task_03-RPG_character/screenshots/scr2.png "Screen 2")

* levelling up the warrior and updating its stats:

    ![Screenshot 3](/Week_02/Assignments/Java_Task_03-RPG_character/screenshots/scr3.png "Screen 3")

