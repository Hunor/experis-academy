import characters.Hero;
import characters.Slot;
import factories.*;
import items.armors.Armor;
import items.weapons.Weapon;

public class Main {
    public static void main(String[] args) {

        HeroFactory heroFactory = new HeroFactory();
        WeaponFactory weaponFactory = new WeaponFactory();
        ArmorFactory armorFactory = new ArmorFactory();  
        
        Hero hero = heroFactory.createHero("warrior");
        Weapon sword = weaponFactory.createWeapon("sword");
        Weapon bow = weaponFactory.createWeapon("bow");
        Armor plate = armorFactory.createArmor("plate");

        hero.setName("Asterix the Brave");
        System.out.println("\n");

        // hero without weapon and armor with basic stats
        System.out.println("Hero without weapon and armor");
        System.out.println("------------------------------");
        System.out.println(hero);
        
        System.out.println("\n");

        // hero with a sword as a weapon 
        System.out.println("Hero with a sword as a weapon");
        System.out.println("------------------------------");
        hero.equipWeapon(sword);
        System.out.println(hero);
        System.out.println(sword);
        
        System.out.println("\n");

        // hero with a bow as a weapon 
        System.out.println("Hero with a bow as a weapon");
        System.out.println("------------------------------");
        hero.equipWeapon(bow);
        System.out.println(hero);
        System.out.println(bow);

        System.out.println("\n");
        
        // hero with a sword and plate 
        // weapon and armor stats are added to basic stats
        System.out.println("Hero with a sword and a plate");
        hero.equipWeapon(sword);
        hero.equipArmor(plate, new Slot[]{Slot.BODY});
        System.out.println("------------------------------");
        System.out.println(hero);
        System.out.println(sword);

        System.out.println("\n");

        // getting from level 1 to level 2 by adding 100 XP points 
        hero.calculateXPForLevelChange(100);

        System.out.println("Hero with a sword and a plate after leveling up");
        System.out.println("------------------------------------------------");
        System.out.println(hero);

        System.out.println("\n");
        
        // hero with a bow and plate 
        // weapon and armor stats are added to basic stats
        System.out.println("Hero with a bow and a plate");
        hero.equipWeapon(bow);
        hero.equipArmor(plate, new Slot[]{Slot.BODY});
        System.out.println("------------------------------");
        System.out.println(hero);
        System.out.println(bow);

        System.out.println("\n");

        // getting from level 2 to level 3 by adding 110 XP points 
        hero.calculateXPForLevelChange(110);

        System.out.println("Hero with a bow and a plate after leveling up");
        System.out.println("------------------------------------------------");
        System.out.println(hero);
        System.out.println(bow);
        System.out.println(plate);


    

    }
}
