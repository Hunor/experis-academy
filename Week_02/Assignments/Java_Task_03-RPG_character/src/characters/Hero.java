package characters;

import java.util.Map;

import items.armors.Armor;
import items.weapons.Weapon;

public interface Hero {
    public void calculateXPForLevelChange(int experiencePoint);
    public Weapon getWeapon();
	public void equipWeapon(Weapon sword);
    public void setName(String name);
    public void equipArmor(Armor armor, Slot[] slots);
    public int getArmorValues(Map<Slot, Object> slot, String point);
    public Map<Slot, Object> getSlotHashMap();
    public double calculateSlotsBonusForArmor(Map<Slot, Object> body);
}
