package characters;

import java.util.HashMap;
import java.util.Map;

import helpers.Helper;
import items.armors.Armor;
import items.weapons.Weapon;
import statslevelup.StatsLevelUp;

public class Mage implements Hero {
    private double mage_health = 100;
    private double mage_strength = 2;
    private double mage_dexterity = 3;
    private double mage_intelligence = 10;

    private String name;
    private int level;
    private int currentXP;
    private int nextLevelXP;

    private Armor armor;
    private Weapon weapon;

    private Map<Slot, Object> slotHashMap;

    public Mage(){
        level = 1;
        currentXP = 0;
        nextLevelXP = 100;
        slotHashMap = new HashMap<Slot, Object>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHealth() {
        return mage_health;
    }

    public void setHealth(double health) {
        this.mage_health = health;
    }

    public double getStrength() {
        return mage_strength;
    }

    public void setStrength(double strength) {
        this.mage_strength = strength;
    }

    public double getDexterity() {
        return mage_dexterity;
    }

    public void setDexterity(double dexterity) {
        this.mage_dexterity = dexterity;
    }

    public double getIntelligence() {
        return mage_intelligence;
    }

    public void setIntelligence(double intelligence) {
        this.mage_intelligence = intelligence;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCurrentXP() {
        return currentXP;
    }

    public int getNextLevelXP() {
        return nextLevelXP;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Map<Slot, Object> getSlotHashMap() {
        return slotHashMap;
    }

    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;

        slotHashMap.put(Slot.WEAPON, weapon);
    }

    public void equipArmor(Armor armor, Slot[] slots) {
        this.armor = armor;

        for (Slot slot : slots) {
            slotHashMap.put(slot, armor);
        }
    }

    public void calculateXPForLevelChange(int experiencePoint) {

        currentXP = currentXP + experiencePoint;

        if (currentXP >= nextLevelXP) {

            setHealth(getHealth() + StatsLevelUp.MAGE_ADD_LEVEL_HEALTH_POINT);
            setStrength(getStrength() + StatsLevelUp.MAGE_ADD_LEVEL_STRENGTH_POINT);
            setDexterity(getDexterity() + StatsLevelUp.MAGE_ADD_LEVEL_DEXTERITY_POINT);
            setIntelligence(getIntelligence() + StatsLevelUp.MAGE_ADD_LEVEL_INTELLIGENCE_POINT);
            
            level++;

            nextLevelXP = (int) (nextLevelXP * 1.1);

            currentXP = 0;

            Helper.calculateWeaponLevelUpPoint(getWeapon());
        }
    }

    public int getArmorValues(Map<Slot, Object> slot, String point) {
        int result = 0;
        for (var entry : slotHashMap.entrySet()) {

            if(entry.getValue() instanceof Armor){
                var slotArmor = ((Armor) entry.getValue());
                if(point.equals("Health")){
                    result += slotArmor.getHealth();
                }
               else if(point.equals("Dexterity")){
                    result += slotArmor.getDexterity();
                }
                else if(point.equals("Strength")){
                    result += slotArmor.getStrength();
                }
                else if(point.equals("Intelligence")){
                    result += slotArmor.getIntelligence();
                }
            }
        }
        return  result;
    }

    public double calculateSlotsBonusForArmor(Map<Slot, Object> body) {
        double result = 0;

        for (var entry : body.entrySet()) {
            if(entry.getValue() instanceof Armor){
                var slot = ((Armor) entry.getValue());
                if (entry.getKey().equals(Slot.HEAD)) {
                    result = result + slot.getStrength() * 0.8;
                } else if(entry.getKey().equals(Slot.LEGS)) {
                    result = result + slot.getStrength() * 0.6;
                } else if(entry.getKey().equals(Slot.BODY)) {
                    result = result + slot.getStrength() * 1;
                }
            }
        }
        return result;
    }

    @Override
    public String toString(){
        return "\n Warrior details: " + name + 
                "\n HP: " + (int)(getHealth() + getArmorValues(slotHashMap, "Health")) + 
                "\n Str: " + (int)(getStrength() + getArmorValues(slotHashMap, "Strength")) +
                "\n Dex: " + (int)(getDexterity() + getArmorValues(slotHashMap, "Dexterity")) + 
                "\n Int: " + (int)(getIntelligence() + getArmorValues(slotHashMap, "Intelligence")) +
                "\n Lvl: " + (int)getLevel() +
                "\n XP to next: " + Helper.getRequiredXP(currentXP, nextLevelXP)
                ;
    }
}
