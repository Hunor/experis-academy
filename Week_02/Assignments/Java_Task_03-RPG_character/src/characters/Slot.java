package characters;

public enum Slot {
    WEAPON,
    BODY,
    LEGS,
    HEAD
}
