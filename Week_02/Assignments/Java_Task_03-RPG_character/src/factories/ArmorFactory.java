package factories;

import items.armors.Armor;
import items.armors.Cloth;
import items.armors.Leather;
import items.armors.Plate;


public class ArmorFactory {
    public Armor createArmor(String armorType) {
        Armor armor = null;
        if( "cloth".equals(armorType) )
        {
            armor = new Cloth();
        }
        else if( "plate".equals(armorType) )
        {
            armor = new Plate();
        }
        else if( "leather".equals(armorType) )
        {
            armor = new Leather();
        } else{
            return null;
        }
        return armor;
    }
}

