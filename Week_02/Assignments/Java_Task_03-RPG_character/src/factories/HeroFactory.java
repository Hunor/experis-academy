package factories;

import characters.Hero;
import characters.Ranger;
import characters.Warrior;

public class HeroFactory {
    public Hero createHero( String heroType )
    {
        Hero hero = null;
        if( "warrior".equals(heroType) )
        {
                hero = new Warrior();
        }
        else if( "ranger".equals(heroType) )
        {
                hero = new Ranger();
        } else {
            return null;
        }
        return hero;
    }
}
