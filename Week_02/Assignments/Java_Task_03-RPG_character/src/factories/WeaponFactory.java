package factories;

import items.weapons.Bow;
import items.weapons.Sword;
import items.weapons.ThunderBolt;
import items.weapons.Weapon;

public class WeaponFactory {
    
    public Weapon createWeapon( String weaponType )
    {
        Weapon weapon = null;
        if( "sword".equals(weaponType) )
        {
            weapon = new Sword();
        }
        else if( "bow".equals(weaponType) )
        {
            weapon = new Bow();
        }
        else if( "thunderbolt".equals(weaponType) )
        {
            weapon = new ThunderBolt();
        } else {
            return null;
        }
        return weapon;
    }
}


