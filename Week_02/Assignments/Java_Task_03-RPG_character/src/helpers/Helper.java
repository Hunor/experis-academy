package helpers;

import items.weapons.MagicWeapon;
import items.weapons.MeleeWeapon;
import items.weapons.RangedWeapon;
import items.weapons.Weapon;

public class Helper {
    
    public static int getRequiredXP(double currentXP, double nextLevelXP) {
        return (int) Math.round(nextLevelXP - currentXP);
    }

    public static void calculateWeaponLevelUpPoint(Weapon weapon) {
        if(weapon instanceof MeleeWeapon) {
            weapon.setLevel(weapon.getLevel() + 1);
            weapon.setAttackDamage((int)(weapon.getAttackModify() + 2));
        } else if(weapon instanceof RangedWeapon) {
            weapon.setLevel(weapon.getLevel() + 1);
            weapon.setAttackDamage((int)(weapon.getAttackModify() + 3));
        } else if(weapon instanceof MagicWeapon) {
            weapon.setLevel(weapon.getLevel() + 1);
            weapon.setAttackDamage((int)(weapon.getAttackModify() + 2));
        }
    }
}
