package items.armors;

public interface Armor {
    String getName();
    double getHealth();
    double getDexterity();
    double getStrength();
    double getIntelligence();
}
