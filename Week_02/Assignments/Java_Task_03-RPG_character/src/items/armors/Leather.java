package items.armors;

import characters.Slot;

public class Leather implements Armor {
    private double health;
    private double intelligence;
    private double dexterity;
    private int level;

    private Slot slot;

    public Leather(){
        health = 20;
        intelligence = 3;
        dexterity = 3;
        level = 1;
    }

    public String getName() {
        return "The Leather of the Lizard King";
    }

    public double getHealth() {
        return health;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public double getDexterity() {
        return dexterity;
    }

    public double getStrength() {
        return 0;
    }

    public String getType() {
        return "Leather";
    }

    public Slot getSlot(){
        return slot;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
