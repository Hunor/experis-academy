package items.armors;

import characters.Hero;
import characters.Slot;

public class Plate implements Armor {
    private double health;
    private double intelligence;
    private double dexterity;
    private int level;

    private Slot slot;

    public Plate(){
        health = 30;
        intelligence = 3;
        dexterity = 1;
        level = 1;
    }

    public String getName() {
        return "The Plate of the last son of Abraham";
    }

    public double getHealth() {
        return health;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public double getDexterity() {
        return dexterity;
    }

    public double getStrength() {
        return 0;
    }

    public String getType() {
        return "Plate";
    }

    public Slot getSlot(){
        return slot;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "\n Item stats for: " + getName() + 
                "\n Armor type: " + getType() +
                "\n Slot: " +
                "\n Armor level: " + getLevel() 
                ;
    }
}
