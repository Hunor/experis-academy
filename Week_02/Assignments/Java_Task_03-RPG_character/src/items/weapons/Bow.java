package items.weapons;

public class Bow implements Weapon, RangedWeapon{
    private int level;
    private double ranged_attack_modifier;

    public Bow(){
        level = 1;
        ranged_attack_modifier = 5;
    }

    public String getName() {
        return "The Bow of Artemis";
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level=level;
    }

    public String getWeaponType() {
        return "Ranged";
    }

    public double getBasicDamage() {
        return ranged_attack_modifier;
    }

    public void setAttackDamage(int attackModifier) {
        ranged_attack_modifier = attackModifier;
    }

    public double getAttackModify() {
        return ranged_attack_modifier;
    }

    @Override
    public String toString(){
        return "\n Item stats for: " + getName() +
                "\n Weapon Type: " + getWeaponType() +
                "\n Weapon level: " + getLevel() + 
                "\n Damage: " + (int)getBasicDamage() 
                ;
    }
}
