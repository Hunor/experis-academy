package items.weapons;

public class Sword implements Weapon, MeleeWeapon {
    private int level;
    private double melee_attack_modifier;

    public Sword(){
        level = 1;
        melee_attack_modifier = 15;
    }

    public String getName() {
        return "The Sword of Hercules";
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getWeaponType() {
        return "Melee";
    }

    public double getBasicDamage() {
        return melee_attack_modifier;
    }
    
    public void setAttackDamage(int attackModifier) {
        melee_attack_modifier = attackModifier;
    }

    public double getAttackModify() {
        return melee_attack_modifier;
    }

    @Override
    public String toString(){
        return "\n Item stats for: " + getName() +
                "\n Weapon Type: " + getWeaponType() +
                "\n Weapon level: " + getLevel() + 
                "\n Damage: " + (int)getBasicDamage() 
                ;
    }
}
