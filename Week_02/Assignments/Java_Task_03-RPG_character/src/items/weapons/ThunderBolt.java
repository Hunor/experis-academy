package items.weapons;

public class ThunderBolt implements Weapon, MagicWeapon {

    private int level;
    private double magic_attack_modifier;

    public ThunderBolt(){
        level = 1;
        magic_attack_modifier = 25;
    }

    public String getName() {
        return "The Thuderbolt of Jupiter";
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level=level;
    }

    public String getWeaponType() {
        return "Magic";
    }

    public double getBasicDamage() {
        return magic_attack_modifier;
    }
    
    public void setAttackDamage(int attackModifier) {
        magic_attack_modifier = attackModifier;
    }

    public double getAttackModify() {
        return magic_attack_modifier;
    }

    @Override
    public String toString(){
        return "\n Item stats for: " + getName() +
                "\n Weapon Type: " + getWeaponType() +
                "\n Weapon level: " + getLevel() + 
                "\n Damage: " + (int)getBasicDamage() 
                ;
    }
}
