package items.weapons;

public interface Weapon {
    public String getName();
    public int getLevel();
    public void setLevel(int level);
    public String getWeaponType();
    public double getBasicDamage();
    public void setAttackDamage(int modify);
    public double getAttackModify();
}
