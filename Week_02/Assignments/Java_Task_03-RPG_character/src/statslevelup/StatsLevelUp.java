package statslevelup;

public class StatsLevelUp {

    public final static double WARRIOR_ADD_LEVEL_HEALTH_POINT = 30;
    public final static double WARRIOR_ADD_LEVEL_STRENGTH_POINT = 5;
    public final static double WARRIOR_ADD_LEVEL_DEXTERITY_POINT = 2;
    public final static double WARRIOR_ADD_LEVEL_INTELLIGENCE_POINT = 1;

    public final static double RANGER_ADD_LEVEL_HEALTH_POINT = 20;
    public final static double RANGER_ADD_LEVEL_STRENGTH_POINT = 2;
    public final static double RANGER_ADD_LEVEL_DEXTERITY_POINT = 5;
    public final static double RANGER_ADD_LEVEL_INTELLIGENCE_POINT = 1;

    public final static double MAGE_ADD_LEVEL_HEALTH_POINT = 15;
    public final static double MAGE_ADD_LEVEL_STRENGTH_POINT = 1;
    public final static double MAGE_ADD_LEVEL_DEXTERITY_POINT= 2;
    public final static double MAGE_ADD_LEVEL_INTELLIGENCE_POINT = 5;
}
