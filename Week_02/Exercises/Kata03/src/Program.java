import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Program {
    public static void main(String[] args) throws ParseException {

        // create an object which will represent the first Friday 13th in January
        String dateString = "01/13/2019";

        // formats and parses dates or time in a language-independent manner
        // The date is represented as a Date object 
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        
        // The class Date represents a specific instant in time
        Date date = dateFormat.parse(dateString);

        // Calendar provides a class method, getInstance, for getting a generally useful object of this type. 
        // Calendar's getInstance method returns a Calendar object whose calendar fields have been initialized with the current date and time
        Calendar cal = Calendar.getInstance();
        
        // Sets this Calendar's time with the given Date
        cal.setTime(date);


        int counter = 0;
        int month = 0;

        // loop through the each month
        while(month <= 12){
            // check if the 13th day of the month is equal to Friday
            // if yes increase the counter
           if(Calendar.FRIDAY == cal.get(Calendar.DAY_OF_WEEK)){
              counter++;
           }

           //Adds or subtracts the specified amount of time to the given calendar field, 
           // based on the calendar's rules
           cal.add(Calendar.MONTH, 1);
           
           month++;
        }
    
        // displaying the number of Firday the 13th in a year
        System.out.println("Number of Fridays on 13th = "+ counter);
    }
}