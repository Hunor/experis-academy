# KATA05: You shall not pass

Create a method to check the strength of a password passed as a parameter. There are 5 criteria which determine the strength of a password. Nordic letters are not catered for simplicity.

 A password is said to be strong if it satisfies the following criteria:
* It contains at least one lowercase English (non-Nordic) character.
* It contains at least one uppercase English (non-Nordic) character.
* It contains at least one special character. 
* Its length is at least 8.
* It contains at least one digit.

If the password matches 3-4 of the above criteria it is moderate and if it matches only 1-2 it is weak.
If the password is less than 6 characters long, or contains white spaces, return invalid. 

## Optional upgrade: 

Tell the user why their password was weak, i.e. what they still need to do to make it strong. You can break design convention for this and simply println() in the check password strength method.
Hint: You can make use of .charAt() to get the character from the password and there are some methods Java has to help with this. You can also make use of Regex to do this.


## Examples:

password -> “Weak”

11081992 -> “Weak”

@S3cur1ty -> “Strong”

!@!pass1 -> “Moderate”

mySecurePass123 -> “Moderate”
