import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the password");
        String inputPassword = scan.next();
        scan.close();
        isValidPassword(inputPassword);
	}
	
    public static boolean isValidPassword(String password)
    {		
    	    // counting variables
    		int a = 0;
    		int b = 0;
    		int c = 0;
    		int d = 0;
    		int e = 0;
    		
            boolean isValid = true;
            if (password.length() > 15 || password.length() < 8)
            {
                    System.out.println("Password must be less than 20 and more than 8 characters in length.");
                    return false;
            }else {
            	a=1;
            }
            
            String upperCaseChars = "(.*[A-Z].*)";
            if (!password.matches(upperCaseChars ))
            {
                    System.out.println("Password must have at least one uppercase character");
                    isValid = false;
                    b=0;
            }else {
            	b=1;
            }
            String lowerCaseChars = "(.*[a-z].*)";
            if (!password.matches(lowerCaseChars ))
            {
                    System.out.println("Password must have at least one lowercase character");
                    isValid = false;
                    c=0;
            }else {
            	c=1;
            }
            String numbers = "(.*[0-9].*)";
            if (!password.matches(numbers ))
            {
                    System.out.println("Password must have at least one number");
                    isValid = false;
                    d=0;
            } else {
            	d=1;
            }
           
            String specialChars = "(.*[@,#,$,%].*$)";
            if (!password.matches(specialChars ))
            {
                    System.out.println("Password must have at least one special character among @#$%");
                    isValid = false;
                    e = 0;
            } else {
            	e = 1;
            }
            
            int total = a+b+c+d+e;
            
            System.out.println(total);
            
            if(a+b+c+d+e >= 4) {
            	System.out.println("Password is Moderate");
            } else {
            	System.out.println("Password is Weak");
            }
            return isValid; 
    }
}
