# KATA06: Refactor legacy code

This is a bit of a different kata, it is designed for us to practice some SOLID principles. You will be given a scenario and some code to work from.

What is expected is that you look through the given classes and identify what SOLID principles they are breaking. The line and the principle. You are then to fix the classes to be more SOLID. There will be one or two violations – so not major refactors just a line or and whatever additions you need to make (other classes, interfaces, …).
The important thing is to not change the public facing API – meaning that the classes still need to produce the same output, so no functionality change. You do not have to demonstrate their use – nothing needs to be written in main(). You are just changing how these few classes interact to be more SOLID (loose coupling, high cohesion).

Scenario: Tire pressure monitoring system

This is a simple system where there is an Alarm class designed to monitor the tire pressure and set an alarm if the pressure falls outside of the expected range. The sensor class provided simulates the behavior of a real tire sensor, providing random but realistic values.

The code for the scenario can be found here: 

https://gitlab.com/NicholasLennox/kata-solid-starting

## Note: 

There are two packages: 
The first is a scenario where you will find the classes. You are to comment where you think a SOLID principle is broken and explain why and what the principle was.
The second is where your solution will go. The classes will be copy pasted there with your changes and additions included. There should be comments showing what you changed where possible.
