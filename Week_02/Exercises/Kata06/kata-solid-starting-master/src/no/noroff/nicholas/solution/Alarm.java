package no.noroff.nicholas.solution;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    Sensor _sensor;
    boolean alarmOn = false;

    public Alarm() {
    }

    // Dependency injection through constructor
    public Alarm(Sensor _sensor) {
        this._sensor = _sensor;
    }
    
    public void check(){
        double psiPressureValue = _sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
