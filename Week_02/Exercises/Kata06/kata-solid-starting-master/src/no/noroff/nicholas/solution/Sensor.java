package no.noroff.nicholas.solution;

public interface Sensor {
    double popNextPressurePsiValue();
}
