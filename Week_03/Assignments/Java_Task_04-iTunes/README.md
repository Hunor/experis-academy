# Task4: Please don't sue me apple.  :minidisc:

A simple web application written in Java Spring and Thymeleaf using Chinook Sqlite database.

## Technologies:
- Backend: Java, Spring
- Frontend: Thymeleaf
- Database: SQLite
- Deployment: Heroku, Docker

## Links:

* Link to `Heroku` where the web application is hosted: https://javaitunes.herokuapp.com
* Link to `javaitunes` container at `Docker Hub`: https://hub.docker.com/r/hunor/javaitunes 

## Endpoints:

| Endpoint        | HTTP Method  | 
|---------------|---------------|
| '/'           |   GET         |
| '/search'     |    GET        |
|  '/api/customers' |  GET |
| '/api/customers' | POST |
| '/api/customers/:id' | PUT |
| '/api/customers/countries/top' | GET |
| '/api/customers/spenders/top' | GET |
| '/api/customers/{id}/popular/genre' | GET |


## Functionalities:
* the Home page displays 5 random artists, 5 random songs and 5 random genres 
* the Home page contains a Search bar that is used to search for tracks. The tracks 
are displayed in a seperate page called search
* The API endpoints cater the following functionalities:
  * read all the customers from the database (displaying: Id, First Name, Last Name, 
  Country, Postal Code, Phone Number and Email)
  * add a new customer to the database
  * update an existing customer
  * get the number of customers in each country in descedning order
  * get customers who are the highest spenders in descending order
  * get the most popular genre for a given customer

## Screenshots of endpoints in Postman:

* Get Home page with Endpoint '/' and HTTP 'GET' verb:

    ![Screenshot 1](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr1-get-homepage.png "Screen 1")

* Get all customers with Endpoint '/api/customers' and HTTP 'GET' verb:

    ![Screenshot 2](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr2-get-all-customers.png "Screen 2")

* Create a new customer with Endpoint '/api/customers' and HTTP 'POST' verb:

    ![Screenshot 3](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr3-create-new-customer.png "Screen 3")

* Update an existing customer with Endpoint '/api/customers/:id' and HTTP 'PUT' verb:

    ![Screenshot 4](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr4-update-customer.png "Screen 4")

* Get updated customer with Endpoint '/api/customers' and HTTP 'GET' verb:

    ![Screenshot 5](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr5-get-updated-customer.png "Screen 5")

* Get countries with most customers with Endpoint '/api/customers/countries/top' and HTTP 'GET' verb:

    ![Screenshot 6](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr6-get-countries-with-most-customers.png "Screen 6")

* Get countries with highest spenders with Endpoint '/api/customers/spenders/top' and HTTP 'GET' verb:

    ![Screenshot 7](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr7-get-countires-with-most-spenders.png "Screen 7")

* Get a customer with favorite genre with Endpoint '/api/customers/{id}/popular/genre' and HTTP 'GET' verb:

    ![Screenshot 8](/Week_03/Assignments/Java_Task_04-iTunes/screenshots/scr8-get-customer-with-favourite-genre.png "Screen 8")

