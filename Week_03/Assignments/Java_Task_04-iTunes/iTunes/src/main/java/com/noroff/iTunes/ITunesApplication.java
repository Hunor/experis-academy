package com.noroff.iTunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITunesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ITunesApplication.class, args);
	}
}
