package com.noroff.iTunes.controllers;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.noroff.iTunes.data_access.CustomerByCountryRepository;
import com.noroff.iTunes.data_access.CustomerBySpendingRepository;
import com.noroff.iTunes.data_access.CustomerPopularGenreRepository;
import com.noroff.iTunes.data_access.CustomerRepository;
import com.noroff.iTunes.models.Customer;
import com.noroff.iTunes.models.CustomerByCountry;
import com.noroff.iTunes.models.CustomerBySpending;
import com.noroff.iTunes.models.Genre;

@CrossOrigin
@RestController
public class CustomerController {

    @GetMapping("/api/customers")
    public ArrayList<Customer> getAllCustomers() {
        return CustomerRepository.getAllCustomers();
    }

    @PostMapping("/api/customers")
    public ResponseEntity newCustomer(@RequestBody Customer newCustomer){
        
    	boolean created = CustomerRepository.addNewCustomer(newCustomer);
        
    	if (created){
            return new ResponseEntity(HttpStatus.CREATED);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/api/customers/{id}")
    public ResponseEntity updateCustomer(@PathVariable String id, @RequestBody Customer updatedCustomer){
        
    	boolean created = CustomerRepository.updateCustomer(updatedCustomer, id);
        
    	if (created){
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/api/customers/countries/top")
    public ArrayList<CustomerByCountry> getCustomersByCountries() {
        return CustomerByCountryRepository.getCustomersByCountry();
    }

    @GetMapping("/api/customers/spenders/top")
    public ArrayList<CustomerBySpending> getCustomersByHighestSpenders() {
        return CustomerBySpendingRepository.getCustomerBySpendings();
    }

    @GetMapping("/api/customers/{id}/popular/genre")
    public ArrayList<Genre> getSpecificCustomersMostPopularGenre(@PathVariable String id) {
        return CustomerPopularGenreRepository.getMostPopularGenre(id);
    }
}
