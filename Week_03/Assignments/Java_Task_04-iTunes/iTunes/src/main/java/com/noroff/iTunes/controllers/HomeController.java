package com.noroff.iTunes.controllers;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.noroff.iTunes.data_access.ArtistRepository;
import com.noroff.iTunes.data_access.GenreRepository;
import com.noroff.iTunes.data_access.SearchRepository;
import com.noroff.iTunes.data_access.TrackRepository;
import com.noroff.iTunes.models.Search;
import com.noroff.iTunes.models.TrackInfo;

@Controller
public class HomeController {
    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("artists", ArtistRepository.getFiveRandomArtists());
        model.addAttribute("tracks", TrackRepository.getFiveRandomTracks());
        model.addAttribute("genres", GenreRepository.getFiveRandomGenres());
        model.addAttribute("search", new Search());
        return "home";
    }

    @GetMapping("/search")
    public String search(Model model, Search search){
        ArrayList<TrackInfo> searchResults = SearchRepository.searchForTrack(search.getQuery());
        model.addAttribute("search", search.getQuery());
        if (searchResults.size() == 0){
            model.addAttribute("nothing", true);
        } else {
            model.addAttribute("tracks", searchResults);
        }
        return "search";
    }
}
