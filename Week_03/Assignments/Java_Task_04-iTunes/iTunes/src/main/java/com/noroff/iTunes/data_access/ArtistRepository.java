package com.noroff.iTunes.data_access;

import java.sql.*;
import java.util.ArrayList;

import com.noroff.iTunes.models.Artist;

public class ArtistRepository {
	private  static Connection connection = null;

	public static ArrayList<Artist> getFiveRandomArtists(){
	    ArrayList<Artist> artists = new ArrayList<Artist>();
	
	    try {
	        connection = DriverManager.getConnection(ConnectionHelper.URL);
	        PreparedStatement statement = connection.prepareStatement("SELECT ArtistId, Name FROM artist ORDER BY RANDOM() LIMIT 5");
	        ResultSet results = statement.executeQuery();
		    while (results.next()){
		        artists.add(new Artist(results.getString("ArtistId"), results.getString("Name")));
		        }
    	} catch (SQLException exception){
    		System.out.println(exception.getMessage());
    		exception.printStackTrace();
    	} finally {
    		try {
				connection.close();
			} catch (SQLException exception) {
				System.out.println(exception.getMessage());
				exception.printStackTrace();
			}
        }
	    return  artists;
	}
}
