package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.CustomerByCountry;

public class CustomerByCountryRepository {
    private static Connection connection = null;

    public static ArrayList<CustomerByCountry> getCustomersByCountry(){
        ArrayList<CustomerByCountry> customers = new ArrayList<>();

        try{
            String query = "SELECT Country, Count(CustomerId) as numberOfCustomers";
            query +=       " FROM customer GROUP BY Country";
            query +=       " ORDER BY Count(CustomerId) DESC";

            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement preparedStatement =
            		connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new CustomerByCountry(
                            resultSet.getString("Country"),
                            resultSet.getInt("numberOfCustomers")
                        )
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
        finally {
            try {
            	connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return customers;
    }
}
