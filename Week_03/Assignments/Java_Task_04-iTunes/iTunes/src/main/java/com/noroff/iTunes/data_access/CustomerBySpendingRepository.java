package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.CustomerBySpending;

public class CustomerBySpendingRepository {
    private static Connection connection = null;

    public static ArrayList<CustomerBySpending> getCustomerBySpendings(){
        ArrayList<CustomerBySpending> spendings = new ArrayList<>();

        try{
            String query = "SELECT c.CustomerId, c.FirstName, c.LastName,";
            query +=          " (SELECT SUM(i.Total) FROM invoice i";
            query +=            " WHERE i.CustomerId = c.CustomerId) as sumOfTotal ";
            query +=       " FROM customer c INNER JOIN invoice i ";
            query +=       " WHERE c.CustomerId = i.CustomerId";
            query +=       " GROUP BY c.CustomerId ORDER BY sumOfTotal DESC";

            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spendings.add(
                        new CustomerBySpending(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getDouble("sumOfTotal")
                        )
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
            	connection.close(); }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return spendings;
    }
}
