package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.Genre;

public class CustomerPopularGenreRepository {
    private static Connection connection = null;

    public static ArrayList<Genre> getMostPopularGenre(String id){
        ArrayList<Genre> genres = new ArrayList<>();

        try{
        	connection = DriverManager.getConnection(ConnectionHelper.URL);
            
            String query = "SELECT genre.GenreId as GenreId, genre.Name as GenreName, COUNT(genre.genreId) AS total FROM genre";
            query +=       " INNER JOIN track ON track.GenreId = genre.genreId";
            query +=       " INNER JOIN invoiceline ON invoiceline.TrackId = track.TrackId";
            query +=       " INNER JOIN invoice ON invoiceline.InvoiceId = invoice.InvoiceId";
            query +=       " INNER JOIN customer ON invoice.CustomerId = customer.CustomerId";
            query +=       " WHERE customer.CustomerId = ? GROUP BY genre.GenreId ORDER BY total DESC";

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            int total = 0;
            while (resultSet.next()) {
                if(resultSet.getInt("total") > total) {
                    total = resultSet.getInt("total");
                }

                if(resultSet.getInt("total") == total) {
                    genres.add(new Genre(
                    		resultSet.getString("GenreId"),
                    		resultSet.getString("GenreName")
                    ));
                } else {
                    break;
                }
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
            	connection.close();           }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return genres;
    }
}
