package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.Customer;

public class CustomerRepository {
    private static Connection connection = null;

    public static boolean updateCustomer(Customer updatedCustomer, String id){
        boolean updated = false;
        try {
            String customerQuery = "UPDATE customer  SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?";
            
            connection = DriverManager.getConnection(ConnectionHelper.URL);

            PreparedStatement statement = connection.prepareStatement(customerQuery);
            statement.setString(1, updatedCustomer.getFirstName());
            statement.setString(2, updatedCustomer.getLastName());
            statement.setString(3, updatedCustomer.getCountry());
            statement.setString(4, updatedCustomer.getPostCode());
            statement.setString(5, updatedCustomer.getPhoneNumber());
            statement.setString(6, updatedCustomer.getEmailAddress());
            statement.setString(7, id);
            statement.executeUpdate();

            updated = true;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
            updated = false;

        } finally {

            try {
            	connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return updated;
    }

    public static boolean addNewCustomer(Customer newCustomer){
        
    	boolean added;
        
    	try {
            String sqlQuery = "INSERT INTO customer(FirstName, LastName, Country, PostalCode, Phone, Email, SupportRepId) VALUES(?,?,?,?,?,?,?)";
            
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            
            PreparedStatement statement = connection.prepareStatement(sqlQuery);

            statement.setString(1, newCustomer.getFirstName());
            statement.setString(2, newCustomer.getLastName());
            statement.setString(3, newCustomer.getCountry());
            statement.setString(4, newCustomer.getPostCode());
            statement.setString(5, newCustomer.getPhoneNumber());
            statement.setString(6, newCustomer.getEmailAddress());
            statement.setString(7, "1");
            statement.executeUpdate();
            added = true;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
            added = false;
        } finally {

            try {
            	connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return added;
    }


    public static ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> allCustomers = new ArrayList<Customer>();
        
        String sqlQuery = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer";
        
        try {
        	connection = DriverManager.getConnection(ConnectionHelper.URL);
            
        	PreparedStatement statement = connection.prepareStatement(sqlQuery);
            
        	ResultSet results = statement.executeQuery();
           
        	while (results.next()){
                String customerID = results.getString("CustomerId");
                String firstName = results.getString("FirstName");
                String lastName = results.getString("LastName");
                String country = results.getString("Country");
                String postCode = results.getString("PostalCode");
                String phoneNumber = results.getString("Phone");
                String emailAddress = results.getString("Email");
                Customer newCustomer = new Customer(customerID, firstName, lastName, country, postCode, phoneNumber, emailAddress);
                allCustomers.add(newCustomer);
            }
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
            	connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return allCustomers;
    }
}
