package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.Genre;

public class GenreRepository {
    private static Connection connection = null;

    public static ArrayList<Genre> getFiveRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>();

        try{
        	connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement preparedStatement =
            		connection.prepareStatement("SELECT GenreId, Name FROM genre ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString("GenreId"),
                                resultSet.getString("Name")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
            	connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return genres;
    }
}
