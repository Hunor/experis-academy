package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.TrackInfo;

public class SearchRepository {
    private static Connection connection = null;

    public static ArrayList<TrackInfo> searchForTrack(String name){
        ArrayList<TrackInfo> matchingTracks = new ArrayList<>();

        String query = "SELECT track.Name as trackName, artist.Name as artistName, album.Title as albumTitle, genre.Name as genreName";
        query +=       " FROM track NATURAL JOIN album INNER JOIN artist on album.ArtistId = artist.ArtistID INNER JOIN genre on genre.GenreId = track.GenreId";
        query +=       " WHERE UPPER(track.Name) LIKE ('%"+name+"%')";
        
        try {
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet results = statement.executeQuery();

            while (results.next()){
                String trackName = results.getString("trackName");
                String artistName = results.getString("artistName");
                String albumName = results.getString("albumTitle");
                String genreName = results.getString("genreName");

                TrackInfo track = new TrackInfo(trackName, artistName, albumName, genreName);
                matchingTracks.add(track);
            }

        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        if (name == null || name.length() == 0 || name.equals(" ")){
            matchingTracks = new ArrayList<>();
        }
        return matchingTracks;
    }
}
