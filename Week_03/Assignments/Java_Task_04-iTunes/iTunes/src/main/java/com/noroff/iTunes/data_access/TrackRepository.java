package com.noroff.iTunes.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.noroff.iTunes.models.Track;

public class TrackRepository {
    private  static Connection connection = null;

    public static ArrayList<Track> getFiveRandomTracks(){
        ArrayList<Track> tracks = new ArrayList<Track>();

        try {
        	connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement("SELECT TrackId, Name FROM track ORDER BY RANDOM() LIMIT 5");
            ResultSet results = statement.executeQuery();

            while (results.next()){
                tracks.add(new Track(results.getString("TrackId"), results.getString("Name")));
            }
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
            	connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return tracks;
    }
}
