package com.noroff.iTunes.models;

public class Customer {
    private final String customerID;
    private final String firstName;
    private final String lastName;
    private final String country;
    private final String postCode;
    private final String phoneNumber;
    private final String emailAddress;

    public Customer(String customerID, String firstName, String lastName,
                    String country, String postCode, String phoneNumber, String emailAddress){
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postCode = postCode;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    public String getCustomerID() {
        return customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() {
        return country;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    public String getEmailAddress() {
    	return emailAddress;
    }
}
