public class SumDigProd {
    public static void sumDigProd(int... args){
        int number = 0;

        for (int i : args)  
          number += i;  
  
        recursive(number);
    }
    //https://www.geeksforgeeks.org/program-to-calculate-product-of-digits-of-a-number/
    static void recursive(int number){
        // Finding the first digit 
        int first = number / 10; 
        //System.out.println(first);

        // Finding the last digit 
        int last = number % 10;
        //System.out.println(last);

        // Finding the multiplied product of the two digits 
        int multipliedProduct = first * last; 
        //System.out.println(multipliedProduct);

        // check if multipliedProduct is one digit if NOT start recursive
        if(multipliedProduct > 0 & multipliedProduct < 10){
            System.out.println(multipliedProduct);
        } else {
            recursive(multipliedProduct);
        }
    }


}
