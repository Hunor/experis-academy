

public class Program {
    public static void main(String args[]){
        String finalWord = "beautiful";
        
        String input1 = "butl";
        String input2 = "butlz";
        String input3 = "tulb";
        String input4 = "bbutl";

        System.out.println(canComplete(input1, finalWord));
        System.out.println(canComplete(input2, finalWord));
        System.out.println(canComplete(input3, finalWord));
        System.out.println(canComplete(input4, finalWord));
    }

    public static boolean canComplete(String inputString, String finalWord){
        if(inputString == null) throw new IllegalArgumentException();
        if(finalWord == null) throw new IllegalArgumentException();
        
        inputString = inputString.toLowerCase();
        finalWord = finalWord.toLowerCase();
        
        int matchFoundCount = 0;

        inputString = inputString.toLowerCase();
        finalWord = finalWord.toString();
        char[] inputStringArr = inputString.toCharArray();
        char[] finalWordArr = finalWord.toCharArray();

        for (int i = 0; i < finalWordArr.length; i++) {
            if (matchFoundCount < inputStringArr.length) {
                if(finalWordArr[i] == inputStringArr[matchFoundCount]){
                    matchFoundCount++;
                }
            } else {
                break;
            }
        }

        return inputString.length() == matchFoundCount;
    }  
}