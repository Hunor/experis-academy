public class Program {
    public static void main(String args[]) {
        /*assertEqual(”00:00:00″, TimeFormat.GetReadableTime(0));
        assertEqual(”00:00:05″, TimeFormat.GetReadableTime(5));
        assertEqual(”00:01:00″, TimeFormat.GetReadableTime(60));
        assertEqual(”23:59:59″, TimeFormat.GetReadableTime(86399));
        assertEqual(”99:59:59″, TimeFormat.GetReadableTime(359999));*/
        
        System.out.println(GetReadableTime(0));
        System.out.println(GetReadableTime(5));
        System.out.println(GetReadableTime(60));
        System.out.println(GetReadableTime(86399));
        System.out.println(GetReadableTime(359999));
    }


    public static String GetReadableTime(int seconds) {
        // %02d = format the integer with 2 digits, left padding it with zeroes
        return String.format("%02d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 60));
    }   
}