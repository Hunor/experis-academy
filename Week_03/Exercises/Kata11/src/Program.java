import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Program {
    public static void main(String args[]){
        String rgb1 = "rgb(0, 0, 0, 0)";
        String rgb2 = "rgba(50,255,0,0.5)";
        validRGB(rgb1);
        validRGB(rgb2);
    }

    public static void validRGB(String input) {

        if (input.startsWith("rgb(")) {
            String regexRGB = "^rgb\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*\\)$";
            Pattern patternRGB = Pattern.compile(regexRGB);
            Matcher matcherRGB = patternRGB.matcher(input);
    
            boolean rgbBool = matcherRGB.matches();
            if (rgbBool) {
                System.out.println("RGB is valid");
            } else {
                System.out.println("RGB is not valid");
            }
        }

        String regexRGB = "^rgb\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*\\)$";
        Pattern patternRGB = Pattern.compile(regexRGB);
        Matcher matcherRGB = patternRGB.matcher(input);

        boolean rgbBool = matcherRGB.matches();
        if (rgbBool) {
            System.out.println("RGB is valid");
        } else {
            System.out.println("RGB is not valid");
        }


        String regexRGBA = "^rgba\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*((0.[1-9])|[01])\\s*\\)$";
        
        Pattern patternRGBA = Pattern.compile(regexRGBA);
        Matcher matcherRGBA = patternRGBA.matcher(input);

        boolean rgbaBool = matcherRGBA.matches();
        if (rgbaBool) {
            System.out.println("RGBA is valid");
        } else {
            System.out.println("RGBA is not valid");
        }

    }

}