package com.noroff.moviecharacterapi.models;

@Entity
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    private String gender;

    // OPTIONAL: private String alias;

    private String picture;
    
    @Column(name = "picture", length = 1000)
    private byte[] picture;

}