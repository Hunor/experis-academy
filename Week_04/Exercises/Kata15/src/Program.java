class Program{
    public static void main(String args[]){
        RomanNumeralsToDecimalConverter converter = new RomanNumeralsToDecimalConverter();
 
        String string1 = "MMVI";
        String string2 = "MCMXLIV";

        System.out.println("Integer form of Roman Numeral is " + converter.romanNumeralToDecimalConverter(string1));
        System.out.println("Integer form of Roman Numeral is " + converter.romanNumeralToDecimalConverter(string2));

    }
}