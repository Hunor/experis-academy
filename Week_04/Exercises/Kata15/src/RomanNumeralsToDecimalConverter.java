public class RomanNumeralsToDecimalConverter {
    
    public int getIntForRomanNumeral(char character)
    {
        if (character == 'I')
            return 1;
        if (character == 'V')
            return 5;
        if (character == 'X')
            return 10;
        if (character == 'L')
            return 50;
        if (character == 'C')
            return 100;
        if (character == 'D')
            return 500;
        if (character == 'M')
            return 1000;
        return -1;
    }

    public int romanNumeralToDecimalConverter(String string)
    {
        int result = 0;
 
        for (int i = 0; i < string.length(); i++) 
        {
            int string1 = getIntForRomanNumeral(string.charAt(i));
 
            if (i + 1 < string.length()) 
            {
                int string2 = getIntForRomanNumeral(string.charAt(i + 1));
 
                if (string1 >= string2) 
                {
                    result = result + string1;
                } else {
                    result = result + string2 - string1;
                    i++;
                }
            } else {
                result = result + string1;
            }
        }
        return result;
    }
}
