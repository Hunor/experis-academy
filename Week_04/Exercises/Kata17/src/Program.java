class Program{
    public static void main(String args[]){
        int n = 670;
        System.out.printf("%10d = '%s'%n", n, numberToWordConverter(n));

        int n1 = 627;
        System.out.printf("%10d = '%s'%n", n1, numberToWordConverter(n1));

        int n2 = 67;
        System.out.printf("%10d = '%s'%n", n2, numberToWordConverter(n2));

    }

    public static String numberToWordConverter(int n){
        final String[] units = {
            "", "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
            "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
        };

        final String[] tens = {
                "",        // 0
                "",        // 1
                "twenty",  // 2
                "thirty",  // 3
                "forty",   // 4
                "fifty",   // 5
                "sixty",   // 6
                "seventy", // 7
                "eighty",  // 8
                "ninety"   // 9
        };

        String result = "";

        if (n < 20) {
            return result = units[n];
        }

        if (n < 100) {
            return result = tens[n / 10] + ((n % 10 != 0) ? " " : "") + units[n % 10];
        }

        if (n < 1000) {
            return result = units[n / 100] + " hundred" + ((n % 100 != 0) ? " " : "") + numberToWordConverter(n % 100);
        }

        return result;
    }
}