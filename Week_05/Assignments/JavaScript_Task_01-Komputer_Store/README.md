# Javascript Task01 - Komputer Shop :computer:

![Screenshot 1](/Week_05/Assignments/JavaScript_Task_01-Komputer_Store/screenshots/screenshot.png "Screen 1")

## The Bank: :bank:
<b>Balance</b>: The bank shows a “Bank” balance in your currency. This is the amount available to you to buy a laptop
bank1

<b>Get a loan</b>: The Get a loan button will attempt to get a loan from the bank. When the get loan is clicked, it must show a “Prompt” popup box that allows you to enter an amount.
bank2

Constraints:
* You cannot get a loan more than double of your bank balance:

* You cannot get more than one bank loan before buying a computer:

## The Work: :office:
<b>Pay</b>:
The pay amount in your currency. Should show how much money you have earned by “working”. This money is NOT part of your bank balance.

<b>Bank button</b>:
The bank button must transfer the money from your Pay balance, to your Bank balance. Remember to reset your pay once you transfer.
Constraints:
* If you have an outstanding loan, 10% of your salary MUST first be deducted and transferred to the outstanding Loan amount

* The balance after the 10% deduction may be transferred to your bank account

<b>Work button</b>:
The work button must increase your Pay balance by 100 on each click. 

<b>Repay loan button</b>:
Once you have a loan, a new button should appear. Upon clicking this button, the full value of your current Pay amount should go towards the outstanding loan and NOT your bank account.

## Laptops: :computer:

<b>Laptop selection</b>:
Use a select box to show at least 4 different laptops. Each laptop must have a unique name and price. You should find a way to store a name, price, description, feature list and image link for each laptop. The feature list of the selected laptop must be displayed here. Changing a laptop should update the user interface with the information for that selected laptop

<b>Info section</b>:
The large box at the bottom or the Info section is where the image, name and description as well as the price of the laptop must be displayed.

<b>Buy Now button</b>:
The buy now button is the final action of your website. This button will attempt to “Buy” a laptop and validate whether the bank balance is sufficient to purchase the selected laptop.
If you do not have enough money in the “Bank”, a message must be shown that you cannot afford the laptop.
When you have sufficient “Money” in the account, the amount must be deducted from the bank and you must receive a message that you are now the owner of the new laptop!
