// get DOM elements
const elCurrentAccount = document.getElementById('currentAccount');
const elBtnApplyForLoan = document.getElementById('btnApplyForLoan');
const elSalaryAccount = document.getElementById('salaryAccount');
const elBtnTransferMoney = document.getElementById('btnTransferMoney');
const elBtnWork = document.getElementById('btnWork');
const elLaptopList = document.getElementById('laptopList');
const elLaptopFeatures = document.getElementById('laptopFeatures');
const elBtnBuy = document.getElementById('btnBuy');
const elLaptopImage = document.getElementById('laptopImage')
const elLaptopName = document.getElementById('laptopModel');
const elLaptopCPU = document.getElementById('laptopCPU');
const elLaptopScreenSize = document.getElementById('laptopScreenSize');
const elLaptopHarddrive = document.getElementById('laptopHarddrive');
const elLaptopDescription = document.getElementById('laptopDescription');
const elLaptopPrice = document.getElementById('laptopPrice');
const elOutstandingLoanAmount = document.getElementById('outstandingLoanAmount');


let previousLoan = false;
let currentAccount = 100;
let salaryAccount = 50;
let loanPaidBack = false;
let amountToLoan = 0;

function myFunction() {
    let i = "vivobook";
    if("vivobook" === laptops[0].id) {
        elLaptopName.innerText = laptops[0].name;
        elLaptopCPU.innerText = laptops[0].cpu;
        elLaptopScreenSize.innerText = laptops[0].screensize;
        elLaptopHarddrive.innerText = laptops[0].harddrive;
        elLaptopDescription.innerText = laptops[0].description;
        elLaptopPrice.innerText = laptops[0].price;
        elLaptopImage.src = laptops[0].image;
        elLaptopFeatures.innerText = laptops[0].description;
    }
}

// Apply for a loan if eligable
elBtnApplyForLoan.addEventListener('click', function(event) {
    // get user input for loan
    amountToLoan  = prompt("Please input the amount you want to borrow.");
    
    // cannot get a loan more than double of bank balance
    if(amountToLoan > (currentAccount * 2)) {
        alert("The amount you want to borrow is too high! You cannot borrow more than the double amount of your current sum of money!");
    } else if (previousLoan){ // cannot get more than one bank loan
        alert("You have already taken a loan from our Bank! You need to buy a laptop before asking for another loan!");
    } else if (amountToLoan <= (currentAccount * 2) && !previousLoan) {
        currentAccount += parseInt(amountToLoan);
        alert(`You have borrowed the amount of EU. ${amountToLoan},-. Your new balance is Euro ${currentAccount},-`); 
        document.getElementById("payLoanButton").innerHTML = '<button class="btn btn-danger" type="button" onclick="payLoan()">Pay loan</button>';
        document.getElementById("outstandingLoanAmount").innerHTML = `<span>Loan amount: Euro ${amountToLoan},- </span>`;
        previousLoan = true;
    }
    // update user interface 
    elCurrentAccount.innerText = currentAccount;
});

// pay back loan
function payLoan(){
    if(salaryAccount > 0) {
        //currentAccount += salaryAccount;
        amountToLoan = amountToLoan - salaryAccount;
        salaryAccount = 0;
    }
    // update user interface
    elSalaryAccount.innerText = salaryAccount;
    //elCurrentAccount.innerText = currentAccount;
    elOutstandingLoanAmount.innerText = amountToLoan;
    document.getElementById("outstandingLoanAmount").innerHTML = `<span>Loan amount: Euro ${amountToLoan},- </span>`;

}

// Earn salary by work. 100 Euro per click
elBtnWork.addEventListener("click", function (event) {
    if(!previousLoan & (amountToLoan==0)){ 
        salaryAccount = salaryAccount + 100;
    } else if (previousLoan & (amountToLoan > 0)){
        salaryAccount = salaryAccount + 90;
        amountToLoan = amountToLoan - 10;
    }
    if(amountToLoan == 0){
        loanPaidBack = true;
        previousLoan = false; 
    }
    // update user interface
    elSalaryAccount.innerText = salaryAccount;
    document.getElementById("outstandingLoanAmount").innerHTML = `<span>Loan amount: Euro ${amountToLoan},- </span>`;
});

// Transfer earned salary to bank 
elBtnTransferMoney.addEventListener('click', function(event) {
    if(salaryAccount > 0) {
        currentAccount += salaryAccount;
        salaryAccount = 0;
    } 
    // update user interface
    elSalaryAccount.innerText = salaryAccount;
    elCurrentAccount.innerText = currentAccount;
});

// Show features of selected laptop 
elLaptopList.addEventListener('click', function(event) {
    showSelectedLaptop();
});

$(document).ready(function () {
    $('#laptopList').change(function () {
        $('#laptopList option:selected');
        let laptop = selectLaptopFromList();
        $('#laptopFeatures').html(laptop.description);
        showSelectedLaptop();
    });
});

// select one laptop from list
function selectLaptopFromList(){
    let selectedLaptop = elLaptopList;
    let laptop = {};
    for(let i = 0; i < laptops.length; i++) {
        if(selectedLaptop.value === laptops[i].id) {
            laptop = laptops[i];
        }
    }
    return laptop;
}


// show info for selected laptop
function showSelectedLaptop() {

    let laptop = selectLaptopFromList();
    
    elLaptopName.innerText = laptop.name;
    elLaptopCPU.innerText = laptop.cpu;
    elLaptopScreenSize.innerText = laptop.screensize;
    elLaptopHarddrive.innerText = laptop.harddrive;
    elLaptopDescription.innerText = laptop.description;
    elLaptopPrice.innerText = laptop.price;
    elLaptopImage.src = laptop.image;
}

// buy laptop
elBtnBuy.addEventListener('click', function(){
    let laptop = selectLaptopFromList();

    if(currentAccount >= laptop.price){
        currentAccount -= laptop.price;
        alert(`You have just purchased ${laptop.name}. Your current balance is ${currentAccount},-`);
        previousLoan = false;
    } else {
        alert('You do not have enough money to buy laptop!');
    }
    elCurrentAccount.innerText = currentAccount;

});

// list of laptops
const laptops = [
    { 
        name: "Asus VivoBook", screensize: "15\"", cpu: "AMD 3000X", harddrive: "SSD-512GB", price: 600, 
        id: "vivobook", description: "The bold and youthful VivoBook series is designed to represent who you are.",
        image: "images/img1.jpg"
    },
    { 
        name: "Lenovo IdeaPad", screensize: "14\"", cpu: "AMD 3000Y", harddrive: "SSD-1028GB", price: 650, 
        id: "ideapad", description: "The IdeaPad design marked a deviation from the business-oriented ThinkPad laptops, towards a more consumer-oriented look and feel.",
        image: "images/img2.jpg"},
    { 
        name: "HP ProBook", screensize: "15\"", cpu: "i5 4900k", harddrive: "SSD-512GB", price: 700,
        id: "probook", description: "The HP ProBook x360 435 delivers the power, security, and durability your growing business demands in a versatile 360° design.",
        image: "images/img3.jpg"},
    { 
        name: "MacBook Pro", screensize: "15\"", cpu: "i7 5700", harddrive: "SSD-512GB", price: 750,
        id: "macbook", description: "Adapts throughout the day, with four use modes that enable you to create, present, and collaborate in a comfortable way.", 
        image: "images/img4.jpg"}
];