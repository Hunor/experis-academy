/*
Create a function that takes a number as an argument. Add up all the numbers from 1 to the number you passed to the function. For example, if the input is 4 then your function should return 10 because 1 + 2 + 3 + 4 = 10.
Examples
addUp(4) ➞ 10
addUp(13) ➞ 91
addUp(600) ➞ 180300
*/

let n = 4;
let n1 = 13;
let n2 = 600;

function addUp(n){
    let result = 0;
    for (let index = 1; index <= n; index++) {
        result += index;
        
    }
    console.log(result);
}

addUp(n);
addUp(n1);
addUp(n2);