function sortDrinkByPrice(drinks){
    return drinks.sort((x,y) => x.price < y.price ? -1 : 1);
}


 function sum(a, b) {
    return a + b;
  }

module.exports = {sortDrinkByPrice, sum}

