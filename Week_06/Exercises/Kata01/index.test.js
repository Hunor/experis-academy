const {sum, sortDrinkByPrice} = require('./index');


test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});



test('sorts drinks by price in array', () => {
    drinks = [
        {name: "lemonade", price: 50},
        {name: "lime", price: 10}
    ]
    expect(sortDrinkByPrice(drinks)).toEqual([{"name": "lime", "price": 10},{"name": "lemonade", "price": 50}]);
});
