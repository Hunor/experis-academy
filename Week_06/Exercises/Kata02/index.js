function noStrangers(input){
    
    let lowerCase = input.toLowerCase();
   
    let withoutDots = lowerCase.replace(/\./g,'');

    let inputAsArray = withoutDots.split(" ");
  
    let wordOccurence = countOccurrences(inputAsArray);
    
    let eachWordOccurence = Object.entries(wordOccurence)

    const friends = [];
    const acquantances = [];

    for (const [word, count] of eachWordOccurence) {

        if(count >= 3 && count < 5){
            acquantances.push(word);
        } else if(count >= 5) {
            friends.push(word);
        }
    }
    return {acquantances, friends};
}

function countOccurrences(arr) {
    return arr.reduce((a, b) => (a[b] = (a[b] || 0) + 1, a), {});
}

noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.");
