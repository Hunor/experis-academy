const chosenWine = require('./index.js');

test('returns 2nd most expensive wine', () => {
    
    chosenWine1 = [
        { name: "Wine A", price: 8.99 },
        { name: "Wine 32", price: 13.99 },
        { name: "Wine 9", price: 10.99 }
    ];
    
    expect(chosenWine(chosenWine1)).toBe("Wine 9");
});

test('returns only item if object has one item', () => {
    
    chosenWine2 = [{ name: "Wine A", price: 8.99 }];
    
    expect(chosenWine(chosenWine2)).toBe("Wine A");
}); 

test('returns null if object is empty', () => {
    
    chosenWine3 = [];
    
    expect(chosenWine(chosenWine3)).toBe(null);
}); 