// https://edabit.com/challenge/ujzhzyvGoASKxSAib

/*
chosenWine([
{ name: "Wine A", price: 8.99 },
{ name: "Wine 32", price: 13.99 },
{ name: "Wine 9", price: 10.99 }
]) ➞ "Wine 9"

chosenWine([{ name: "Wine A", price: 8.99 }]) ➞ "Wine A"

chosenWine([]) ➞ null
*/

let chosenWine1 = ([
    { name: "Wine A", price: 8.99 },
    { name: "Wine 32", price: 13.99 },
    { name: "Wine 9", price: 10.99 }
]);

let chosenWine2 = ([{ name: "Wine A", price: 8.99 }]);

let chosenWine3 = ([]);

function chosenWine(input){
    // empty
    if(input.length === 0){
        return null;
    }

    // only one
    if(input.length === 1){
        return input[0].name;
    }

    // more than one choice
    if(input.length > 1){
        return input.sort((a,b) => a.price-b.price)[1].name;
    }
};


//chosenWine(chosenWine);
module.exports = chosenWine;