const guitarTuner = require('./index');

test('ceck guitar tunes', () => {
    let stringFrequency = [329.63,246.94,196.00,146.83,110.00,82.41];

    expect(guitarTuner(stringFrequency)).toBe([" - ", "OK", " - ", " - ", " - ", ">>•"]);
  });