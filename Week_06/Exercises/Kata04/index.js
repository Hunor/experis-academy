let stringFrequencyTune = [0, 246.94, 0, 0, 0, 80];

function guitarTuner(stringFrequencyTune){

    let stringFrequency = [329.63,246.94,196.00,146.83,110.00,82.41];

    return stringFrequencyTune.map(function(tune, reference){
        //let result = [];
        
        if(tune == 0){
            return ' - ';
        }

        let differenceInTunePercent = Math.round(tune / stringFrequency[reference] * 100) - 100;

        if (differenceInTunePercent >= 3) return '•<<';

		if (differenceInTunePercent >= 1) return '•<';

		if (differenceInTunePercent <= -3) return '>>•';

		if (differenceInTunePercent <= -1) return '>•';

        return 'OK';
    });
}

guitarTuner(stringFrequencyTune);

module.exports = guitarTuner;