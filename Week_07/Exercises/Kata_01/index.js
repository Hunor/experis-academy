/*The .length property on an array will return the number of elements in the array. For example, the array below contains 2 elements:
[1, [2, 3]]
// 2 elements, number 1 and array [2, 3]
Suppose we instead wanted to know the total number of non-nested items in the nested array. In the above case, [1, [2, 3]] contains 3 non-nested items, 1, 2 and 3.
Write a function that returns the total number of non-nested items in a nested array.
Examples
getLength([1, [2, 3]]) ➞ 3
getLength([1, [2, [3, 4]]]) ➞ 4
getLength([1, [2, [3, [4, [5, 6]]]]]) ➞ 6
getLength([1, [2], 1, [2], 1]) ➞ 5
Notes
An empty array should return 0.*/

/*function getLength(array){
    let sum = 0;
    for (let count = 0; count < array.length; count ++) {
      sum += Array.isArray(array[count]) ? getLength(array[count]) : 1;
    }
    return sum;
}

console.log(getLength([1, [2, 3]]));
console.log(getLength([1, [2, [3, [4, [5, 6]]]]]));*/


// array .flat()
/*let array = [1, [2, 3]];
console.log(array.flat());

let array1 = [1, [2, [3, [4, [5, 6]]]]];
console.log(array1.flat());

console.log(array1.flat(Infinity).length);*/

// array .toString() and string .toSplit()
let array2 = [1, [2, 3]];
console.log(array2.toString());
console.log(array2.toString().split(','));
console.log(array2.toString().split(',').length);