function validName(name) {
    
    let nameArray = name.split(" ");

	for(let i = 0; i < nameArray.length; i++){
        
        let term = nameArray[i];

        // Rule: not upper case
        if( term[0] !== term[0].toUpperCase()){ 
            console.log('Not valid: Not uppercase');
        } else if( term.includes(".") && term.length > 2){  // Rule: word has a dot          
            console.log('Not valid: word has a dot');
        } else if( term.length === 1){ // Rule: inital has no dot
            console.log('Not valid: inital has no dot');
        };
	};

    // Rule: only term or more than 3
	if( nameArray.length < 2 || nameArray.length > 3){
		console.log('Not valid: only term or more than 3');
	} else if ( nameArray[nameArray.length-1].includes(".")){ // Rule: last name must be a word (not an initial) 
        console.log('Not valid: last name must be a word (not an initial)');
	} else if ( nameArray[0].includes(".") === true &&     // Rule: 3 terms and only first term initial
                nameArray[1].includes(".") === false &&
                nameArray.length > 2){
        console.log('Not valid: 3 terms and only first term initial');
	};
	console.log('Valid!');
}

validName("H. Wells"); // ➞ true

validName("H. G. Wells"); // ➞ true

validName("Herbert G. Wells"); // ➞ true

validName("Herbert"); // ➞ false
// Must be 2 or 3 words

validName("h. Wells"); // ➞ false
// Incorrect capitalization

validName("H Wells"); // ➞ false
// Missing dot after initial

validName("H. George Wells"); // ➞ false
// Cannot have: initial first name + word middle name

validName("H. George W."); // ➞ false
// Last name cannot be initial

validName("Herb. George Wells"); // ➞ false
// Words cannot end with a dot (only initials can)