// https://edabit.com/challenge/EhGY9aaNHiCqqpnL9

function happy(number) {

    let sum = 0;

    for (let item of number.toString()) {
        
        item = parseInt(item)
        
        item = Math.pow(item, 2);
        
        sum += item
        
        console.log(`${n}:${item}`)
    }

    // Any happy number will have a 1 in its sequence, 
    // and every unhappy number will have a 4 in its sequence.
    if(sum === 1){
        return true;
    } else if (sum === 4){
        return false;
    } else {
        return happy(sum);
    }
}

happy(203);