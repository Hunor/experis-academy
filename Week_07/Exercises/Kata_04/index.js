// https://edabit.com/challenge/5hsyLC2Ntgoqn2wAy


function findAllDigits(nums) {
	var found = [];

	for(var i = 0; i < nums.length; i++){

		var number = Array.from(nums[i].toString());

		for(j = 0; j < number.length; j++){
			if(!found.includes(number[j])){
				found.push(number[j]);
			}
	  }
		if(found.length == 10){
            //console.log(nums[i]);

			return nums[i];
		}
	}
    console.log('missing');
}

findAllDigits([5175, 4538, 2926, 5057, 6401, 4376, 2280, 6137, 8798, 9083]);
