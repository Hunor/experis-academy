# JavaScript - Pokemon Trainer :video_game:

A Pokemon catalogue written in Angular framework using Pokemon API https://pokeapi.co

## Landing page 

The user aka. trainer must start by putting in his name. The name is stored locally. 

## Trainer page

The trainer page displays all the Pokemon the Trainer has collected. It displays a list of Pokemon
with their Avatar. Each item is clickable and takes the user to the Pokemon Detail page for specific Pokemon that was clicked.

## Pokemon catalogue

Trainer cannot have access to this page unless he has entered a trainer name.

The catalogue contains a list card style Pokemon. The Pokemon have an image and name displayed.
Each Pokemon card is clickable and takes the user to Pokemon detail page.

## Pokemon detail

The detail profile contains the following information:
* base stats: image, types, base stats, name
* profile: height, weight, abilities, base experience
* moves: list of moves

# Getting started

* clone the repository
* run `npm install` to install the necessary dependencies
* run `ng serve --open` to run the application in the default browser 
