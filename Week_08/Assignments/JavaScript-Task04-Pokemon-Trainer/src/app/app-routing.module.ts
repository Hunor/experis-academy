import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { AuthGuard } from './guards/auth.guards';

const routes: Routes = [{
  path: 'header',
    component: HeaderComponent
  },
  {
    path: 'trainer',
    loadChildren: () => import('./components/trainer/trainer.module').then(module => module.TrainerModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogue',
    loadChildren: () => import('./components/catalogue/catalogue.module').then(module => module.CatalogueModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'pokemon/:pokemonId',
    loadChildren: () => import('./components/detail/detail.module').then(module => module.DetailModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/header'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/header'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
