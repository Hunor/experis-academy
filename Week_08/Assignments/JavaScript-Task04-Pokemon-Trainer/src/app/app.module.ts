import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { DetailComponent } from './components/detail/detail.component';
import { TrainerComponent } from './components/trainer/trainer.component';

@NgModule({
  declarations: [	
    AppComponent,
    HeaderComponent,
    CatalogueComponent,
    DetailComponent,
    TrainerComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
