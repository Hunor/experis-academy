import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonImage } from 'src/app/models/pokemonImage.model';
import { ApiService } from 'src/app/services/api/api.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  @Input()
  pokemon: Pokemon | undefined;
  @Output() pokemonName: EventEmitter<string> = new EventEmitter();
  pokemonList: Pokemon[] = []
  selectedPokemon: any 
  constructor(private router: Router, private apiService: ApiService, private storageService: StorageService) { }

  async ngOnInit() {
    try {
      const result: any = await this.apiService.getPokemon()
      this.pokemonList = result.results || []
      this.pokemonList.forEach((pokemon)=> {
        const imageUrl: PokemonImage = this.apiService.getPokemonImage(pokemon.url)
        pokemon.sprites = imageUrl
      })
    } catch (error) {
      console.error('ERROR: ', error)
    } finally {
      console.log('PokemonList: ', this.pokemonList)
    }
  }

  async onPokemonClicked(pokemon: Pokemon) {
    try {
      const result: any = await this.apiService.getPokemonById(pokemon.name)
      this.selectedPokemon = result
      console.log("Chosen Pokemon", this.selectedPokemon)
      this.storageService.savePokemon(this.selectedPokemon)
      this.router.navigateByUrl(`/pokemon/${this.selectedPokemon.name}`)
    } catch (error) {
      console.error("ERROR: ", error)
    }
  }

}
