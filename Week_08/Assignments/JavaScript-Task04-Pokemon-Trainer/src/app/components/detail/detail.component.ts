import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  pokemon: Pokemon = this.storage.getPokemon()

  @Input() captured: any

  constructor(private router: Router,
    private storage: StorageService) { }

  ngOnInit() {

  }

  onCapturePokemonClicked() {
    if(confirm(`You are catching ${this.pokemon.name}`)) {
      this.storage.saveCapturedPokemon(this.pokemon)
      this.router.navigateByUrl('/catalogue')
      window.alert(`\n${this.pokemon.name.toUpperCase()} was caught!`)
    }
  }

}
