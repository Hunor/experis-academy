import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import * as uuid from 'uuid';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private session: SessionService,
    private router: Router) {
    if (this.session.getLoginToken() !== false) {
    this.router.navigateByUrl('/catalogue')
    }
  }

  user = {
    token: uuid.v4(),
    username: ''
  }

  isLoading: boolean = false

  ngOnInit() {
  }

  onRegisterClicked() {
    if(this.user.username !== '') {
      this.session.save(this.user.token, this.user.username)
    } else {
      window.alert('You need a name!')
    }
    this.router.navigateByUrl('/catalogue')
  }
}
