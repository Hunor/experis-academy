import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  capturedPokemon: any = this.storage.getCapturedPokemon()

  constructor(private storage: StorageService,
    private router: Router) { 
  }

  ngOnInit() {
  }
  onPokemonSelected(pokemon: Pokemon) {
    this.storage.savePokemon(pokemon)
    this.router.navigateByUrl(`/pokemon/${pokemon.name}`)
  }
}
