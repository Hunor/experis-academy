import { PokemonImage } from '../models/pokemonImage.model'
import { Abilities } from '../models/abilities.model'
//import { Type } from '../models/type.model'

export interface Pokemon {
    name: string,
    url: string,
    sprites: PokemonImage,
    types: any[],
    stats: any[],
    height: number,
    weight: number,
    abilities: Abilities,
    base_experience: number,
    moves: any[],
}