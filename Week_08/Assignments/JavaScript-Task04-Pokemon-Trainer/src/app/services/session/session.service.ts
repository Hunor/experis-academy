import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }
  save(session: string, username: string) {
    localStorage.setItem('login_token', session)
    localStorage.setItem('username', username)
  }

  getLoginToken(): any {
    const savedSession = localStorage.getItem('login_token')
    
    return savedSession ? savedSession : false
  }
}
