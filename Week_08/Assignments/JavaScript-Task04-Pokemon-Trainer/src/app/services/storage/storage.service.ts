import { Injectable } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  capturedPokemon: Pokemon[] = []
  constructor() { }

  savePokemon(pokemon: Pokemon): any {
    localStorage.setItem('pokemon', JSON.stringify(pokemon))
  }

  getPokemon(): Pokemon {
    const storedPokemon = JSON.parse(localStorage.getItem('pokemon') || '{}')
    return storedPokemon ? storedPokemon : null
  }

  saveCapturedPokemon(pokemon: Pokemon): any {
    this.capturedPokemon.push(pokemon)
    localStorage.setItem('capturedPokemon', JSON.stringify(this.capturedPokemon))
  }

  getCapturedPokemon(): Pokemon {
    const capturedPokemon = JSON.parse(localStorage.getItem('capturedPokemon') || '{}')
    return capturedPokemon ? capturedPokemon : null
  }
  
}
