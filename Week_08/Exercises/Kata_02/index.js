// https://edabit.com/challenge/jsukwFKLKJx2qLs5b

function ticTacToe(board) {
	
	if(board[0][0] == board[0][1] && board[0][1] == board[0][2]){
		return board[0][0]
	}
	
    if(board[1][0] == board[1][1] && board[1][1] == board[1][2]){
        return board[0][0]
	}
	
    if(board[2][0] == board[2][1] && board[2][1] == board[2][2]){
        return board[0][0]
	}
	
    if(board[0][0] == board[1][0] && board[1][0] == board[2][0]){
        return board[0][0]
	}
	
    if(board[0][1] == board[1][1] && board[1][1] == board[2][1]){
        return board[0][1]
	}
	
    if(board[0][2] == board[1][2] && board[1][2] == board[2][2]){
        return board[0][2]
	}
	
    if(board[0][2] == board[1][1] && board[1][1] == board[2][0]){
        return board[0][2]
	}
	
    if(board[0][0] == board[1][1] && board[1][1] == board[2][2]){
        return board[0][0]
	}
	
	return "Draw";
	
}

/*
const patterns = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[2, 4, 6]
];

for(let p of patterns) {
    if (

    ) {
        return flat[p[0]];
    }
}
*/ 