/*
Pandigital Numbers
A pandigital number contains all digits (0-9) at least once. Write a function that takes an integer, returning true if the integer is pandigital, and false otherwise.

Examples
isPandigital(98140723568910) ➞ true

isPandigital(90864523148909) ➞ false
// 7 is missing.

isPandigital(112233445566778899) ➞ false
Notes
Think about the properties of a pandigital number when all duplicates are removed.
*/

function isPandigital(input) 
{
    const string = input.toString();

	let count = true;

	for (let i=0; i < 10; i++) {
		if (!string.includes(i.toString())) {
			count = false;
		}
	}
	return count;

    /*var inputArray = [];

    for (var i = 0; i <= input.length; i++) {
        inputArray.push(i);
    }
    console.log(inputArray);

    let digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    for (let i = 0; i < input.length; i++) {
        for (let j = 0; j < array.length; j++) {
            if(){

            }
            
        }
        
    }*/
};

isPandigital(98140723568910);
isPandigital(90864523148909);
isPandigital(112233445566778899);