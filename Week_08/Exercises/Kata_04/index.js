// https://edabit.com/challenge/npLurjMJofmFRCJwx


function letterCombinations(digit) {
	
	let stringDigit = {2:'abc',3:'def',4:'ghi',5:'jkl',6:'mno',7:'pqrs',8:'tuv',9:'wxyz'}
	
    let current = [''];
	
    for (let a = 0; a < digit.length; a++){
		
        let combination = [];

		for (let i =0; i<current.length; i++){
			for (let j =0; j<stringDigit[Number(digit[a])].length;j++){
				combination.push(current[i] + stringDigit[Number(digit[a])][j]);
                
			}
		}
		current = combination;
        console.log(combination);

	}
    console.log(current);
	//return current;
}

letterCombinations("23");